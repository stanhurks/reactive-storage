import React from 'react'
import Storage from './storage/Storage'
import { Subscription } from 'rxjs'

/**
 * The main entry point of the application.
 * 
 * This application is solely made to demonstrate the Storage functionality.
 * 
 * @author Stan Hurks
 */
export default class App extends React.Component {

    /**
     * The subscription for when the Storage updates.
     */
    private subscriptionStorageUpdate!: Subscription

    public componentDidMount = () => {
        Storage.initialize()

        this.subscriptionStorageUpdate = Storage.update.subscribe(this.onStorageUpdate)
    }

    public componentWillUnmount = () => {
        this.subscriptionStorageUpdate.unsubscribe()
    }

    public render = () => {
        return (
            <div id="app">
                <input value={Storage.data.test} type="text" onChange={(event) => {
                    Storage.data.test = event.target.value
                }} />

                <div>
                    <h1>
                        Storage data:
                    </h1>
                    <p>
                        {
                            JSON.stringify(Storage.data)
                        }
                    </p>
                </div>

                <div>
                    <h2>
                        Storage.data.test = {
                            Storage.data.test
                        }
                    </h2>
                </div>
            </div>
        )
    }

    /**
     * Whenever the storage updates
     */
    private onStorageUpdate = () => {

        // Wait for the thread to be done before updating the component
        // with new Storage data.
        setTimeout(() => {
            this.forceUpdate()
        })
    }
}