/**
 * Some dummy class to demonstrate the storage hack functionality.
 * 
 * @author Stan Hurks
 */
export default class StorageDataModelSession {

    /**
     * The session uuid
     */
    public uuid: string = '0000-0000-0000-0000'
}