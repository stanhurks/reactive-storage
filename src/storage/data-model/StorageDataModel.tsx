import StorageDataModelSession from './StorageDataModelSession'

/**
 * The data model to be used when saving the state into Storage.
 * 
 * This is the same model for LocalStorage and SessionStorage.
 * 
 * When the Storage functionality is initialized,
 * the properties of this class will be iterated.
 * 
 * Each property will be prefixed with a `_` and from there
 * getters and setters are generated with the non-prefixed name.
 * 
 * This will cause intelliSense to work like `Storage.data.foo = 'bar'`,
 * while during run-time a custom setter will be executed instead.
 * 
 * This setter will cause the Storage to automatically update in the browser
 * for each change made. :)
 * 
 * @author Stan Hurks
 */
export default class StorageDataModel {

    /**
     * Some property for testing purposes
     */
    public test: string = ''

    /**
     * For demonstration purposes a session model can be put here as well
     * to keep track of session data of the user.
     */
    public session: StorageDataModelSession = new StorageDataModelSession()
}