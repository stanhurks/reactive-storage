/**
 * The utility for all frequently used object manipulations
 * that are too complex for in one line.
 * 
 * @author Stan Hurks
 */
export default class Objects {

    /**
     * Checks whether a value is of type Object
     */
    public static isObject(value: any): boolean {
        return value === Object(value) && Object.prototype.toString.call(value) !== '[object Array]'
    }
}