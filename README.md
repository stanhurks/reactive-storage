# Reactive storage

This project has been created due to my frustrations with the complexity of Redux. This project is based on the react framework.

Feel free to use this software for personal+commercial use, here is how it works:

#
## Running the demo
```
  yarn install && yarn start
```

#
## Creating the data model
In order to create the data model, you just have to add members to the `src/storage/data-model/StorageDataModel.tsx` class.

This is all done by simple and clear OOP.

#
### During runtime
During runtime, when initializing the `src/storage/Storage.tsx` class, using the `Initialize()` method, all members in the `StorageDataModel` will be suffixed by a `_`. Simultaniously getters and setters will be generated. In the setter the value will be set and simultaniously the localstorage/sessionstorage will be updated in the browser.

#
### Why is this useful?
You only have to set-up a OOP data model for all data you wish to save in storage. Because the get/set methods have the same name as the original member had before the runtime, intelliSense is fully supported.

This means that you can access data in your components/pages like so:
```
  Storage.data.session
```
and intelliSense will tell you the respected datatype and its members.

#
### Saving data
To save data, you can simply alter the datamodel like so:
```
  Storage.data.variable = 12
```
And it will be immediately saved in Storage. There is also an `rxjs Subject` that will fire once the Storage is updating: `Storage.update`. So extending this software to automatically update the state of a component should not be too difficult to do.
